﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeldLib.Helpers
{
    class Constants
    {
        public static string APP_VERSION = "0.0.2";
        public static string APP_NAME = "Сварка";
        public static string MAIN_WINDOW_TITLE = APP_NAME + "     v." + APP_VERSION;
    }
}
