﻿using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WeldLib.Models;
using WeldLib.UI.ViewModels;
using WeldLib.UI.Views;

namespace WeldLib
{
    public class WeldSymbolService
    {
        WeldWindowService wwService;

        private MainWindow window;
        private MainViewModel windowVM;
        private SldWorks swApp;

        public WeldSymbolService(SldWorks swApp)
        {
            this.swApp = swApp;
            wwService = new WeldWindowService();
        }


        public void Show()
        {
            if (window == null)
            {
                window = new MainWindow();
                windowVM = new MainViewModel();
                windowVM.ReReadWeldData_Notify += ReReadWeldData;
                windowVM.CloseWeldWindow_Notify += Close;
                windowVM.CreateWeldTable_Notify += CreateWeldTable;
                window.DataContext = windowVM;
                window.Closed += Window_Closed;
            }
            window.Show();
            ReReadWeldData();
        }

        public void Close()
        {
            if (window != null)
            {
                window.Close();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            windowVM.CreateWeldTable_Notify -= CreateWeldTable;
            windowVM.ReReadWeldData_Notify -= ReReadWeldData;
            windowVM.CloseWeldWindow_Notify -= Close;
            window.Closed -= Window_Closed;
            window = null;
            windowVM = null;
        }

        private void ReReadWeldData()
        {
            if (windowVM != null)
            {
                windowVM.LocalData = GetLocalData();
                ObservableCollection<WeldsListModel> result = GetBasicData_NEW();
                windowVM.TableData = ProcessWeldNumbers(ref result);
                windowVM.BasicData = result;
            }
        }

        private string GetLocalData()
        {
            if (IsDrawing())
            {
                return windowVM.BasicData.Count.ToString();
            }

            return "Откройте чертеж.";
        }



        private void SetWeldsInSheet(string sheetName, ObservableCollection<WeldsListModel> welds)
        {
            Debug.Print("welds count {0}", welds.Count);
            bool status = GetDrawing().ActivateSheet(sheetName);
            IView view = (IView)GetDrawing().GetFirstView();
            while (view != null)
            {
                if (view.Type == (int)swDrawingViewTypes_e.swDrawingSheet)
                {
                    view = view.IGetNextView();
                    continue;
                }
                if (view.GetWeldSymbolCount() > 0)
                {
                    GetModel().ClearSelection2(true);
                    SelectionMgr selMgr = GetModel().SelectionManager;

                    foreach (WeldSymbol ws in view.GetWeldSymbols())
                    {
                        selMgr.AddSelectionListObject(ws, null);
                        GetModel().ActivateSelectedFeature();

                        WeldsListModel tmp = welds.Where(
                            x => (
                            x.SheetName.Equals(sheetName)
                            && x.ID.ToString().Equals(ws.GetText(8))
                            )).First();
                        wwService.WriteCreateTableData(tmp);
                    }
                }
                view = view.IGetNextView();
            }
        }

        private ObservableCollection<TableWeldModel> ProcessWeldNumbers(ref ObservableCollection<WeldsListModel> welds)
        {
            HashSet<string> tmp = new HashSet<string>();
            foreach (WeldsListModel wm in welds)
            {
                tmp.Add(wm.TopText);
                tmp.Add(wm.BotText);
            }
            List<string> tmp1 = tmp.ToList();
            foreach (WeldsListModel wm in welds)
            {
                wm.NumberTop = tmp1.IndexOf(wm.TopText) + 1;
                wm.NumberBot = tmp1.IndexOf(wm.BotText) + 1;
            }
            ObservableCollection<TableWeldModel> ww = new ObservableCollection<TableWeldModel>();
            foreach (string item in tmp1)
            {
                ww.Add(new TableWeldModel() { Number = ww.Count + 1, Name = item });
            }
            return ww;
        }
        
        private bool IsDrawing()
        {
            return swApp != null && swApp.ActiveDoc != null && (swApp.ActiveDoc as ModelDoc2).GetType() == (int)swDocumentTypes_e.swDocDRAWING;
        }

        private ModelDoc2 GetModel()
        {
            return swApp.ActiveDoc as ModelDoc2;
        }

        private DrawingDoc GetDrawing()
        {
            if (IsDrawing())
            {
                return swApp.ActiveDoc as DrawingDoc;
            }
            return null;
        }

        private string[] GetSheetsNames()
        {
            if (IsDrawing())
            {
                return (swApp.ActiveDoc as DrawingDoc).GetSheetNames();
            }
            return new string[0];
        }

        private void CreateWeldTable(ObservableCollection<WeldsListModel> welds, ObservableCollection<TableWeldModel> tables)
        {
            TableAnnotation weldTable = GetDrawing().InsertTableAnnotation2(false, 0, 0, 1, "", tables.Count + 1, 2);
            weldTable.TitleVisible = true;
            weldTable.Title = "Таблица 1 - Швы сварных соединений";
            weldTable.Text2[1, 0, true] = "№ шва";
            weldTable.Text2[1, 1, true] = "Обозначение шва";
            for (int i = 0; i < tables.Count; i++)
            {
                weldTable.Text2[i + 2, 0, true] = tables[i].Number.ToString();
                weldTable.Text2[i + 2, 1, true] = tables[i].Name;
            }

            SetWelds(welds);
            Close();
        }


        // ********** REFACTORING ********************
        #region ReadDate
        private ObservableCollection<WeldsListModel> GetBasicData_NEW()
        {
            List<WeldsListModel> result = new List<WeldsListModel>();
            if (IsDrawing())
            {
                string currentSheet = GetDrawing().GetCurrentSheet().GetName();
                foreach (string sn in GetSheetsNames())
                {
                    result.AddRange(GetWeldSymbolListFromSheet_NEW(sn));
                }
                bool status = GetDrawing().ActivateSheet(currentSheet);
            }
            return new ObservableCollection<WeldsListModel>(result);
        }

        private List<WeldsListModel> GetWeldSymbolListFromSheet_NEW(string sheetName)
        {
            List<WeldsListModel> result = new List<WeldsListModel>();

            bool status = GetDrawing().ActivateSheet(sheetName);
            IView view = (IView)GetDrawing().GetFirstView();
            while (view != null)
            {
                if (view.Type == (int)swDrawingViewTypes_e.swDrawingSheet)
                {
                    view = view.IGetNextView();
                    continue;
                }
                if (view.GetWeldSymbolCount() > 0)
                {
                    result.AddRange(GetWeldSymbolListFromView_NEW(view));
                }
                view = view.IGetNextView();
            }
            return result;
        }

        private List<WeldsListModel> GetWeldSymbolListFromView_NEW(IView view)
        {
            List<WeldsListModel> result = new List<WeldsListModel>();

            GetModel().ClearSelection2(true);
            SelectionMgr selMgr = GetModel().SelectionManager;

            foreach (WeldSymbol ws in view.GetWeldSymbols())
            {
                selMgr.AddSelectionListObject(ws, null);
                GetModel().ActivateSelectedFeature();

                int id = result.Count;

                WeldsListModel tmp = new WeldsListModel(id, view.Sheet.GetName(), view.Name);
                wwService.ReadWeldWindowData(ref tmp);
                ws.SetText(true, tmp.TopText, "", tmp.BotText, tmp.SideText, 1);
                ws.SetText(false, "", "", "", id.ToString(), 1);

                result.Add(tmp);
            }
            return result;
        }
        #endregion
        #region CreateTable
        private void SetWelds(ObservableCollection<WeldsListModel> welds)
        {
            if (IsDrawing())
            {
                string currentSheet = GetDrawing().GetCurrentSheet().GetName();
                foreach (string sn in GetSheetsNames())
                {
                    SetWeldsInSheet(sn, welds);
                }
                bool status = GetDrawing().ActivateSheet(currentSheet);
            }
        }

        private void SetWeldsInSheet_NEW(string sheetName, ObservableCollection<WeldsListModel> welds)
        {
            bool status = GetDrawing().ActivateSheet(sheetName);
            IView view = (IView)GetDrawing().GetFirstView();
            while (view != null)
            {
                if (view.Type == (int)swDrawingViewTypes_e.swDrawingSheet)
                {
                    view = view.IGetNextView();
                    continue;
                }
                if (view.GetWeldSymbolCount() > 0)
                {
                    SetWeldsInView_NEW(view, welds);
                }
                view = view.IGetNextView();
            }
        }

        private void SetWeldsInView_NEW(IView view, ObservableCollection<WeldsListModel> welds)
        {
            GetModel().ClearSelection2(true);
            SelectionMgr selMgr = GetModel().SelectionManager;

            foreach (WeldSymbol ws in view.GetWeldSymbols())
            {
                selMgr.AddSelectionListObject(ws, null);
                GetModel().ActivateSelectedFeature();

                WeldsListModel tmp = welds.Where(
                    x => (
                    x.SheetName.Equals(view.Sheet.GetName())
                    && x.ID.ToString().Equals(ws.GetText(8))
                    )).First();
                wwService.WriteCreateTableData(tmp);
            }
        }
        #endregion
        #region RemoveTable

        #endregion
    }
}
