﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WeldLib.Models;

namespace WeldLib
{
    public class WeldWindowService
    {
        #region Windows API
        const int WM_GETTEXT = 0x000D;
        const int WM_SETTEXT = 0x000C;
        const int WM_SYSCOMMAND = 0x0112;
        const int SC_CLOSE = 0xF060;
        const int BM_CLICK = 0x00F5;

        public delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        public static extern int GetClassName(
            IntPtr hwnd,
            [MarshalAs(UnmanagedType.LPStr)] StringBuilder buf,
            int nMaxCount
            );

        [DllImport("user32.dll")]
        public static extern int GetWindowText(
            IntPtr hwnd,
            StringBuilder buf,
            int nMaxCount
            );

        [DllImport("user32.dll", EntryPoint = "SendMessage", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        public static extern bool SendMessage(IntPtr hWnd, uint Msg, int wParam, StringBuilder lParam);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wparam, int lparam);

        [DllImport("user32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr window, EnumWindowProc callback, IntPtr i);
        #endregion

        public void ReadWeldWindowData(ref WeldsListModel model)
        {
            IntPtr res = FindWindow(@"#32770", @"Свойства");
            Debug.Print("search result: {0}", res.ToString());
            if (res.ToInt64() != 0)
            {
                StringBuilder cname = new StringBuilder(256);
                GetClassName(res, cname, cname.Capacity);

                List<IntPtr> edits = GetChildWindows(res);

                var childTitle = new StringBuilder(256);

                SendMessage(edits[0], (int)WM_GETTEXT, childTitle.Capacity, childTitle);
                model.SideText = childTitle.ToString();

                SendMessage(edits[1], (int)WM_GETTEXT, childTitle.Capacity, childTitle);
                model.TopText = childTitle.ToString();

                SendMessage(edits[2], (int)WM_GETTEXT, childTitle.Capacity, childTitle);
                model.BotText = childTitle.ToString();

                SendMessage(res, WM_SYSCOMMAND, SC_CLOSE, 0);
            }
            else
            {
                Debug.Print("window not found");
            }
        }

        public void WriteCreateTableData(WeldsListModel model)
        {
            IntPtr res = FindWindow(@"#32770", @"Свойства");
            if (res.ToInt64() != 0)
            {
                StringBuilder cname = new StringBuilder(256);
                GetClassName(res, cname, cname.Capacity);
                List<IntPtr> edits = GetChildWindows(res);
                StringBuilder weldField;
                if (!string.IsNullOrEmpty(model.TopText))
                {
                    weldField = new StringBuilder("N" + model.NumberTop);
                    SendMessage(edits[1], (int)WM_SETTEXT, weldField.Capacity, weldField);

                }
                if (!string.IsNullOrEmpty(model.BotText))
                {
                    weldField = new StringBuilder("N" + model.NumberBot);
                    SendMessage(edits[2], (int)WM_SETTEXT, weldField.Capacity, weldField);
                }
                SendMessage(edits[3], BM_CLICK, 0, 0);
            }
            else
            {
                Debug.Print("window not found");
            }
        }
        public void WriteRemoveTableData(ref WeldsListModel model)
        {
            IntPtr res = FindWindow(@"#32770", @"Свойства");
            Debug.Print("search result: {0}", res.ToString());
            if (res.ToInt64() != 0)
            {
                StringBuilder cname = new StringBuilder(256);
                GetClassName(res, cname, cname.Capacity);
                List<IntPtr> edits = GetChildWindows(res);
                var childTitle = new StringBuilder(256);
                SendMessage(edits[0], (int)WM_GETTEXT, childTitle.Capacity, childTitle);
                model.SideText = childTitle.ToString();
                SendMessage(edits[1], (int)WM_GETTEXT, childTitle.Capacity, childTitle);
                model.TopText = childTitle.ToString();
                SendMessage(edits[2], (int)WM_GETTEXT, childTitle.Capacity, childTitle);
                model.BotText = childTitle.ToString();
                SendMessage(res, WM_SYSCOMMAND, SC_CLOSE, 0);
            }
            else
            {
                Debug.Print("window not found");
            }
        }


        public static List<IntPtr> GetChildWindows(IntPtr parent)
        {
            List<IntPtr> result = new List<IntPtr>();
            GCHandle listHandle = GCHandle.Alloc(result);
            try
            {
                EnumWindowProc childProc = new EnumWindowProc(EnumWindow);
                EnumChildWindows(parent, childProc, GCHandle.ToIntPtr(listHandle));
            }
            finally
            {
                if (listHandle.IsAllocated)
                    listHandle.Free();
            }
            return result;
        }

        private static bool EnumWindow(IntPtr handle, IntPtr pointer)
        {
            GCHandle gch = GCHandle.FromIntPtr(pointer);
            List<IntPtr> list = gch.Target as List<IntPtr>;
            if (list == null)
            {
                throw new InvalidCastException("GCHandle Target could not be cast as List<IntPtr>");
            }
            StringBuilder className = new StringBuilder(256);
            GetClassName(handle, className, className.Capacity);
            StringBuilder classTitle = new StringBuilder(256);
            GetWindowText(handle, classTitle, classTitle.Capacity);
            if (className.ToString().Equals("Edit") || (className.ToString().Equals("Button") && classTitle.ToString().Equals("ОК")))
            {
                list.Add(handle);
            }
            return true;
        }
    }
}
