﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WeldLib.Models;
using WeldLib.UI.Helpers;
using static WeldLib.Helpers.Constants;

namespace WeldLib.UI.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public delegate void WeldWindowEvent();
        public event WeldWindowEvent CloseWeldWindow_Notify;
        public event WeldWindowEvent ReReadWeldData_Notify;
        public delegate void WeldTableEvent(ObservableCollection<WeldsListModel> welds, ObservableCollection<TableWeldModel> weldTable);
        public event WeldTableEvent CreateWeldTable_Notify;

        public string WindowTitle => MAIN_WINDOW_TITLE;

        private string localData;
        private ObservableCollection<WeldsListModel> basicData;
        private ObservableCollection<TableWeldModel> tableData;

        public MainViewModel()
        {
            this.ReReadDataCommand = new DelegateCommand(o => this.ReReadData());
            this.CreateTableCommand = new DelegateCommand(o => this.CreateTable());
            this.CloseWeldWindowCommand = new DelegateCommand(o => this.CloseWeldWindow());
        }

        #region Props
        public string LocalData
        {
            get => localData ?? "_";
            set => SetProperty(ref localData, value);
        }

        public ObservableCollection<WeldsListModel> BasicData
        {
            get
            {
                if (basicData == null)
                    basicData = new ObservableCollection<WeldsListModel>();
                return basicData;
            }
            set => SetProperty(ref basicData, value);
        }

        public ObservableCollection<TableWeldModel> TableData
        {
            get => tableData ?? new ObservableCollection<TableWeldModel>();
            set => SetProperty(ref tableData, value);
        }
        #endregion

        #region Commands
        public ICommand ReReadDataCommand { get; private set; }
        public ICommand CreateTableCommand { get; private set; }
        public ICommand CloseWeldWindowCommand { get; private set; }
        #endregion
        
        #region methods
        private void ReReadData()
        {
            ReReadWeldData_Notify();
            OnPropertyChanged(nameof(BasicData));
        }

        private void CloseWeldWindow()
        {
            CloseWeldWindow_Notify();
        }
        private void CreateTable()
        {
            CreateWeldTable_Notify(BasicData,TableData);
        }

        #endregion
    }
}
