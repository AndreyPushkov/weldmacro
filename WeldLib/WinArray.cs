﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WeldLib
{
    public class WinArray : ArrayList
    {
        public delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);

        [DllImport("user32.dll", SetLastError = true)]
        static extern int FindWindow(string lpClassName, string lpWindowName);


        private delegate bool EnumWindowsCB(IntPtr hwnd, IntPtr param);

        [DllImport("user32")]
        private static extern int EnumWindows(EnumWindowsCB cb, IntPtr param);



        [DllImport("user32.dll")]
        public static extern int GetClassName(
            IntPtr hwnd,
            [MarshalAs(UnmanagedType.LPStr)] StringBuilder buf,
            int nMaxCount
            );

        [DllImport("user32.dll")]
        public static extern int GetWindowText(
            IntPtr hwnd,
            StringBuilder buf,
            int nMaxCount
            );

        private static bool MyEnumWindowsCB(IntPtr hwnd, IntPtr param)
        {
            GCHandle gch = (GCHandle)param;
            WinArray itw = (WinArray)gch.Target;
            // Afx:0000000140000000:8:0000000000010003:0000000000000000:00000000004B066F

            //StringBuilder cname = new StringBuilder(256);
            //GetClassName(hwnd, cname, cname.Capacity);
            //GetWindowText(hwnd, cname, cname.Capacity);
            //if (cname.ToString().Equals(@"Afx:0000000140000000:8:0000000000010003:0000000000000000:00000000004B066F"))//#32770 (Диалоговое окно)
            //if (cname.ToString().Equals(@"Свойства1"))
            //{
            //    StringBuilder title = new StringBuilder(256);
            //    GetWindowText(hwnd, title, title.Capacity);
            //    //List<IntPtr> rrr = GetChildWindows(hwnd);
            //    Debug.Print("SW title: {0}", title);
            //    //foreach (IntPtr item in rrr)
            //    //{
            //    //    StringBuilder childTitle = new StringBuilder(256);
            //    //    GetWindowText(item, childTitle, childTitle.Capacity);
            //    //    Debug.Print("child title: {0}", childTitle);
            //    //}


            //}
            itw.Add(hwnd);
            return true;
        }

        // Свойства
        //#32770 (Диалоговое окно)

        public WinArray()
        {
            GCHandle gch = GCHandle.Alloc(this);
            EnumWindowsCB ewcb = new EnumWindowsCB(MyEnumWindowsCB);
            EnumWindows(ewcb, (IntPtr)gch);
            //int rr = FindWindow("Windows.UI.Core.CoreWindow", null);
            Debug.Print("find {0}", (gch.Target as WinArray).Count);
            gch.Free();
        }

        [DllImport("user32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr window, EnumWindowProc callback, IntPtr i);

        public static List<IntPtr> GetChildWindows(IntPtr parent)
        {
            List<IntPtr> result = new List<IntPtr>();
            GCHandle listHandle = GCHandle.Alloc(result);
            try
            {
                EnumWindowProc childProc = new EnumWindowProc(EnumWindow);
                EnumChildWindows(parent, childProc, GCHandle.ToIntPtr(listHandle));
            }
            finally
            {
                if (listHandle.IsAllocated)
                    listHandle.Free();
            }
            return result;
        }

        private static bool EnumWindow(IntPtr handle, IntPtr pointer)
        {
            GCHandle gch = GCHandle.FromIntPtr(pointer);
            List<IntPtr> list = gch.Target as List<IntPtr>;
            if (list == null)
            {
                throw new InvalidCastException("GCHandle Target could not be cast as List<IntPtr>");
            }
            list.Add(handle);
            //  You can modify this to check to see if you want to cancel the operation, then return a null here
            return true;
        }
    }
}
