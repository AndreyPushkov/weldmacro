﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeldLib.Models
{
    public class TableWeldModel
    {
        public string Name { get; set; } = "";
        public int Number { get; set; }
    }
}
