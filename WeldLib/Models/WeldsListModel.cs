﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WeldLib.Models
{
    public class WeldsListModel
    {
        public WeldsListModel()
        {
        }

        public WeldsListModel(int iD, string sheetName, string viewName)
        {
            ID = iD;
            SheetName = sheetName;
            ViewName = viewName;
        }

        public int ID { get; set; }
        public string SheetName { get; set; } = "";
        public string ViewName { get; set; } = "";
        public string TopText { get; set; } = "";
        public string BotText { get; set; } = "";
        public string SideText { get; set; } = "";
        public int NumberTop { get; set; }
        public int NumberBot { get; set; }
        public string NumberTopValue => NumberTop > 0 ? NumberTop.ToString() : "";
        public string NumberBotValue => NumberBot > 0 ? NumberBot.ToString() : "";
    }
}
