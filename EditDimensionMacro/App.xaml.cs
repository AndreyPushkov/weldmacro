﻿using EditDimensionMacro.Exeptions;
using EditDimensionMacro.Helpers;
using EditDimensionMacro.Sw;
using EditDimensionMacro.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WeldLib;

namespace EditDimensionMacro
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static SwApp swApp;
        private WeldSymbolService weldService;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            AppUtils.CheckApplicationSingleInstance();
            try
            {
                swApp = AppUtils.GetSwInstance();
                bool attachResult = swApp.AttachEventHandlers();
                if (!attachResult)
                    throw new CustomException("Ошибка инициации слушателей SolisWorks....");
                weldService = new WeldSymbolService(swApp.SwApplication);
                weldService.Show();
            }
            catch (CustomException ce)
            {
                MessageBox.Show(ce.Message);
                AppUtils.ApplicationExit();
            }

        }
    }
}
