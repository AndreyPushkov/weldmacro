﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditDimensionMacro.Helpers
{
    class Constants
    {
        public static string APP_VERSION = "0.0.1";
        public static string APP_NAME = "Заготовка макроса";
        public static string MAIN_WINDOW_TITLE = APP_NAME + "     v." + APP_VERSION;
    }
}
