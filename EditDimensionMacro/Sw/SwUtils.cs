﻿using SolidWorks.Interop.sldworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EditDimensionMacro.Exeptions;
using SolidWorks.Interop.swconst;

namespace EditDimensionMacro.Sw
{
    public static class SwUtils
    {
        private static bool IsSwApplication()
        {
            return App.swApp != null
                ? App.swApp.exists()
                : false;
        }

        private static SldWorks GetSwApplication()
        {
            return IsSwApplication()
                ? App.swApp.SwApplication
                : throw new CustomException("SolidWorks missed");
        }

        public static bool isModel()
        {
            return GetSwApplication().ActiveDoc != null;
        }

        public static ModelDoc2 GetModel()
        {
            return GetSwApplication().ActiveDoc ?? throw new CustomException("Нет открытых документов");
        }

        public static Mouse getMouse()
        {
            return (GetModel().GetFirstModelView() as ModelView).GetMouse();
        }

        public static object getConfigurationNames()
        {
            return GetModel().GetConfigurationNames();
        }

        public static bool isDrawing()
        {
            return GetModel().GetType() == (int)swDocumentTypes_e.swDocDRAWING;
        }

        public static DrawingDoc GetDrawing()
        {
            return isDrawing()
                ? (DrawingDoc)GetModel()
                : throw new CustomException("Это не чертеж");
        }

        public static bool isAssembly()
        {
            return GetModel().GetType() == (int)swDocumentTypes_e.swDocASSEMBLY;
        }

        public static AssemblyDoc GetAssembly()
        {
            return isAssembly()
                ? (AssemblyDoc)GetModel()
                : throw new CustomException("Это не сборка");
        }

        public static bool isPart()
        {
            return GetModel().GetType() == (int)swDocumentTypes_e.swDocPART;
        }

        public static PartDoc GetPart()
        {
            return isPart()
                ? (PartDoc)GetModel()
                : throw new CustomException("Это не деталь");
        }

        public static bool isSelectionMgr()
        {
            return GetSelectionMgr() != null;
        }

        public static SelectionMgr GetSelectionMgr()
        {
            return GetModel().SelectionManager;

        }

        public static dynamic GetSelectedObject()
        {
            return GetSelectionMgr().GetSelectedObject6(1, 0);
        }

        public static int GetSelectedObjectType()
        {
            return isSelectionMgr()
                ? GetSelectionMgr().GetSelectedObjectType3(1, -1)
                : (int)swSelectType_e.swSelNOTHING;
        }

        public static bool isDimension()
        {
            return GetSelectedObjectType() == (int)swSelectType_e.swSelDIMENSIONS;
        }

        public static DisplayDimension GetSelectedDimension()
        {
            return isDimension()
                ? (DisplayDimension)GetSelectionMgr().GetSelectedObject6(1, 0)
                : throw new CustomException("Это не размер");
        }

        public static int getDocumentDimensionPrecision() // точность размеров заданная в документе ( возможно надо раскидать чтение по типам размеров ) 
        {
            return GetModel().Extension.GetUserPreferenceInteger(
                    (int)swUserPreferenceIntegerValue_e.swDetailingLinearDimPrecision,
                    (int)swUserPreferenceOption_e.swDetailingLinearDimension
                    );
        }

        public static int getDocumentTolerancePrecision() // точность допусков заданная в документе ( возможно надо раскидать чтение по типам размеров ) 
        {
            return GetModel().Extension.GetUserPreferenceInteger(
                    (int)swUserPreferenceIntegerValue_e.swDetailingLinearTolPrecision,
                    (int)swUserPreferenceOption_e.swDetailingLinearDimension
                    );
        }
    }
}
