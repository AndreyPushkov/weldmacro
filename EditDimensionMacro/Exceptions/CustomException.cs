﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace EditDimensionMacro.Exeptions
{
    class CustomException : Exception
    {
        public CustomException(String message) : base(message)
        {
        }
    }
}
