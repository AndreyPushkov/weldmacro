﻿using EditDimensionMacro.Sw;
using EditDimensionMacro.Exeptions;
using EditDimensionMacro.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SolidWorks.Interop.sldworks;
using System.Diagnostics;
using System.IO;

namespace EditDimensionMacro.Utils
{
    class AppUtils
    {
        public static string GetApplicationName()
        {
            return Application.ResourceAssembly.GetName().Name;
        }

        public static string GetApplicationSettingsPath()
        {
            //return GetApplicationFolder() + @"\" + Constants.SETTINGS_FILE_NAME;
            return GetApplicationFolder() + @"\";
        }

        public static string GetApplicationFolder()
        {
            return Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
        }

        #region Get SolidWorks instance
        [DllImport("ole32.dll")]
        private static extern int CreateBindCtx(uint reserved, out IBindCtx ppbc);

        public static SwApp GetSwInstance()
        {
            try
            {
                return new SwApp(GetSwApp());
            }
            catch (CustomException ex)
            {
                MessageBox.Show(ex.Message);
                ApplicationExit();
            }
            return null;
        }

        public static SldWorks GetSwApp()
        {
            return (SldWorks)GetSwAppFromProcess() ?? throw new CustomException("SolidWorks не найден");
        }

        private static ISldWorks GetSwAppFromProcess()
        {
            Process[] procs = Process.GetProcessesByName("SLDWORKS");
            if (procs.Length > 0)
            {
                var myPrc = procs[0];

                var monikerName = "SolidWorks_PID_" + myPrc.Id.ToString();

                IBindCtx context = null;
                IRunningObjectTable rot = null;
                IEnumMoniker monikers = null;

                try
                {
                    CreateBindCtx(0, out context);

                    context.GetRunningObjectTable(out rot);
                    rot.EnumRunning(out monikers);

                    var moniker = new IMoniker[1];

                    while (monikers.Next(1, moniker, IntPtr.Zero) == 0)
                    {
                        var curMoniker = moniker.First();

                        string name = null;

                        if (curMoniker != null)
                        {
                            try
                            {
                                curMoniker.GetDisplayName(context, null, out name);
                            }
                            catch (UnauthorizedAccessException)
                            {
                            }
                        }

                        if (string.Equals(monikerName,
                            name, StringComparison.CurrentCultureIgnoreCase))
                        {
                            object app;
                            rot.GetObject(curMoniker, out app);
                            return app as ISldWorks;
                        }
                    }
                }
                finally
                {
                    if (monikers != null)
                    {
                        Marshal.ReleaseComObject(monikers);
                    }

                    if (rot != null)
                    {
                        Marshal.ReleaseComObject(rot);
                    }

                    if (context != null)
                    {
                        Marshal.ReleaseComObject(context);
                    }
                }
            }
            return null;
        }
        #endregion

        #region App Single inctance
        public static void CheckApplicationSingleInstance()
        {
            List<Process> arr = getProcessListByName(GetApplicationName());
            if (arr.Count > 1)
            {
                staySingleInstance(arr);
            }
        }

        private static List<Process> getProcessListByName(string appName)
        {
            Debug.Print("single check " + appName);

            return Process.GetProcesses()
                .Where(x => x.ProcessName.Equals(appName))
                .ToList();
        }

        private static void staySingleInstance(List<Process> arr)
        {
            arr.OrderByDescending(x => x.StartTime)
                .Take(arr.Count() - 1)
                .ToList()
                .ForEach(x => x.Kill());
        }
        #endregion


        #region MainWindow visibility
        public static void ShowMainWindow()
        {
            Application.Current.Dispatcher.Invoke(delegate
            {
                App.Current.MainWindow.Visibility = Visibility.Visible;
            });
        }

        public static void HideMainWindow()
        {
            Application.Current.Dispatcher.Invoke(delegate
            {
                App.Current.MainWindow.Visibility = Visibility.Hidden;
            });
        }

        #endregion

        #region Settings
        //public static SettingsModel ReadSettings()
        //{
        //    return XmlFileManager.read(GetApplicationSettingsPath());
        //}

        //public static void SaveSettings(SettingsModel settings)
        //{
        //    XmlFileManager.save(settings, GetApplicationSettingsPath());
        //}
        #endregion


        public static void ApplicationExit()
        {
            try { }
            finally
            {
                App.swApp.Dispose();
            }
            Application.Current.Dispatcher.Invoke(delegate
            {
                Application.Current.Shutdown();
            });
        }
    }
}
